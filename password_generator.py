import random
import string

characters = string.ascii_letters + string.digits + string.punctuation

password_characters = []


def password_generator():
    global password_characters

    while True:
        try:
            password_length = int(input("Type password length (8 to 20 characters): "))
            if password_length < 8 or password_length > 20:
                print("Please choose number from 8 to 20!")
            else:
                break
        except ValueError:
            print("That was not a required number. Please try again.")

    for number in range(password_length):
        password_characters += random.choice(characters)

    print("".join(password_characters))


password_generator()
