import random
import string

generated_password = "".join(random.choice(string.ascii_letters + string.digits + string.punctuation) for character
                             in range(int(input("Type number (password length): "))))
print(generated_password)